# oc create cm gitlab-env --from-file=envdl.sh --dry-run=client -o yaml | oc apply -f -
# oc label cm gitlab-env controller.devfile.io/mount-to-devworkspace=true controller.devfile.io/watch-configmap=true

cd $PROJECT_SOURCE
GIT_URL=$(git config --get remote.origin.url)

echo Checking $GIT_URL
if [[ ${GIT_URL:0:19} != 'https://gitlab.com/' ]]
then
    echo $GIT_URL is not a gitlab url
    return
fi

AUTH="Authorization: Bearer $(cat /.git-credentials/credentials | sed 's/.*oauth2://' | sed 's/@.*//')"

echo Finding the Project ID fro the API
URLESC=$(printf %s ${GIT_URL:19} | sed 's/\.git.*//' | jq -sRr @uri)
PROJECT_ID=$(curl -XGET --header "$AUTH" "https://gitlab.com/api/v4/projects/$URLESC" | jq .id)
echo Project id is $PROJECT_ID

if [ -z $PROJECT_ID ]
then
    return
fi

ENVIRONMENT=*
echo "Scanning project $PROJECT_ID for $ENVIRONMENT values"
# Get production values within project
for ((i=1; ; i+=1)); do
    echo "page $i"
    vars=$(curl -s --header "$AUTH" "https://gitlab.com/api/v4/projects/$PROJECT_ID/variables?filter\[environment_scope\]=$ENVIRONMENT&page=$i")
    # Check if the latest file response is an empty json array
    if printf %s $vars | jq -e '. | length == 0' >/dev/null; then
       break
    fi
    # Otherwise set these values in the environment
    echo $(printf %s $vars | jq -r '.[] | select(.variable_type == "env_var" and .protected == false and .environment_scope == "*") | "export " + .key + "=" + .value') >> $HOME/.envs
done

echo 'source $HOME/.envs' >> $HOME/.bashrc