curl --netrc-file $HOME/.netrc -o $NX_LICENSE_DIR/nxlm https://artifactory.apps.vader-ocp-nonprod.brighthousefinancial.com/artifactory/bhf-python-repo/nxlm/18.3.1/nxlm

chmod +x $NX_LICENSE_DIR/nxlm

cp $HOME/nx-license $NX_LICENSE_DIR/license.nxcfg

$NX_LICENSE_DIR/nxlm --install $NX_LICENSE_DIR/license.nxcfg

echo 'alias nxlm="$NX_LICENSE_DIR/nxlm"' >> $HOME/.bashrc